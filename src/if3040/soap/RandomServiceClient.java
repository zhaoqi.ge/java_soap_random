package if3040.soap;

public class RandomServiceClient {

  public static void main( String args[] ) {
    try {
      RandomServiceStub random = new RandomServiceStub( "http://localhost:8080/axis2/services/RandomService" );
      if( args.length >= 2 ) {
        RandomServiceStub.SetBounds request = new RandomServiceStub.SetBounds();
        request.setMin( Integer.valueOf( args[0] ));
        request.setMax( Integer.valueOf( args[1] ));
        random.setBounds( request );
      }
      else {
        RandomServiceStub.NextRandom request = new RandomServiceStub.NextRandom();
        for( int i = 0; i < 10; ++i ) {
          RandomServiceStub.NextRandomResponse response = random.nextRandom( request );
          System.out.print( response.get_return() + " " );
        }
        System.out.println();
      }
    }
    catch ( Exception e ) {
      System.err.println( "Client exception: " + e.toString() );
      e.printStackTrace();
    }
  }
}
