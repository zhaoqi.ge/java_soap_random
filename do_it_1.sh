cd java_soap_random

# Download and unzip binary distibution of Axis2 tested with version 1.7.9) on http://axis.apache.org/axis2/java/core/download.cgi
curl -O https://downloads.apache.org/axis/axis2/java/core/1.7.9/axis2-1.7.9-bin.zip
unzip axis2-1.7.9-bin.zip
rm axis2-1.7.9-bin.zip

export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
export AXIS2_HOME=$PWD/axis2-1.7.9

mkdir -p RandomService/if3040/soap
cp src/if3040/soap/RandomService.java RandomService/if3040/soap/
mkdir -p RandomService/META-INF
cp src/services.xml RandomService/META-INF/

cd RandomService
javac if3040/soap/RandomService.java
$AXIS2_HOME/bin/java2wsdl.sh -cp . -cn if3040.soap.RandomService -of META-INF/RandomService.wsdl

cd ..
mv RandomService $AXIS2_HOME/repository/services/
echo RandomService >> $AXIS2_HOME/repository/services/services.list

# remove endorsed.dirs in $AXIS2_HOME/bin/axis2server.sh
sed -i '64d' axis2-1.7.9/bin/axis2server.sh

$AXIS2_HOME/bin/axis2server.sh

# go to: https://<repl-url>.repl.co/axis2/services/RandomService/nextRandom
# or:    https://<repl-url>.repl.co/axis2/services/RandomService/setBounds?args0=-2&args1=2

